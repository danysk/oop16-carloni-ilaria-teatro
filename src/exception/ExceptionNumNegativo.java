package exception;

/**
 * Eccezione lanciata al tentativo di inserimento di un numero negativo durante
 * la prenotazione dei posti
 * 
 * @author Ilaria Carloni
 * 
 */
public class ExceptionNumNegativo extends Exception {

	private static final long serialVersionUID = 1L;

}
