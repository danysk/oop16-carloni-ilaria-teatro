package exception;

/**
 * Eccezione lanciata quando si tenta di prenotare un numero di posti maggiore
 * di quelli disponibili
 * 
 * @author Ilaria Carloni
 * 
 */

public class ExceptionPostiNonDisponibili extends Exception {

	private static final long serialVersionUID = 1L;

}
